<p align="center"> <img src="https://komarev.com/ghpvc/?username=whitehatcyberus&label=Profile%20views&color=0e75b6&style=flat" alt="whitehatcyberus" /> </p><br>
<h1 align="center"><b>Hi , I'm White Hat Cyberus </b><img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="35"></h1>

<p align="center">
  <a href="https://github.com/DenverCoder1/readme-typing-svg"><img src="https://readme-typing-svg.herokuapp.com?font=Time+New+Roman&color=cyan&size=25&center=true&vCenter=true&width=700&height=100&lines=Welcome+To+The+World+of+Ethical+Hacking..&hearts;++;Security+Researcher,;Enterprise+External+Auditor+ISO+27001,;CyberSecurity+Awareness+Speaker/Blogger..<3;HackerOne's+Bug+Bounty+Hunter"></a>
</p>


<br>
<h3 align="center"><a href="https://aspen.eccouncil.org/VerifyBadge?type=training&a=kd0njM3W2TclJ0sO02HQ4A==">EC Council's Certified Ethical Hacker (v11)</a></h3>

- 🎓 Currently pursuing my **Bachelor's of Technology in Computer Science and Engineering** (8th Semester)

- ⚙️ Trained under **Cognizant** as a **CyberSecurity Student** and currently a **HackerOne Ethical Hacker**

- 🌱 I’m currently learning **Vulnerability Assessment, Penetration testing, incident response, reverse engineering and ISO 27001 auditing**

- 👨‍💻 All of my projects are available at [here](https://github.com/WhiteHatCyberus)

- 📝 I regularly write articles on [WordPress Blogs](https://ethicalcyberuspathways.wordpress.com/)

- 💬 Ask me about **cybersecurity trends**  [![Hotmail Badge](https://img.shields.io/badge/Email-whcyberus-brightgreenc14438?style=flat&logo=MicrosoftOutlook&logoColor=green&link=mailto:whcyberus@gmail.com)](mailto:whcyberus@gmail.com)

<picture> <img align="right" src="https://github.com/0xAbdulKhalid/0xAbdulKhalid/raw/main/assets/mdImages/Right_Side.gif" width = 250px></picture>

<br>
<h3 align="center">Connect with me:</h3>
<p align="center">
<a href="https://twitter.com/mattsleety" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="mattsleety" height="30" width="40" /></a><br>
<a href="https://linkedin.com/in/whcyberus" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="whcyberus" height="30" width="40" /></a>
<a href="https://ethicalcyberuspathways.wordpress.com" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/wordpress.svg" alt="whcyberus" height="30" width="40" /></a></br>
<p align="center">

</p>
<br>


-----

<br>


## <img src="https://media.giphy.com/media/iY8CRBdQXODJSCERIr/giphy.gif" width="35"><b> Github Stats </b>
<br>

<div align="center">

<a href="https://github.com/WhiteHatCyberus/">
  <img src="https://github-readme-stats.vercel.app/api?username=WhiteHatCyberus&include_all_commits=true&count_private=true&show_icons=true&line_height=20&title_color=7A7ADB&icon_color=2234AE&text_color=D3D3D3&bg_color=0,000000,130F40" width="450"/>
  <img src="https://github-readme-stats.vercel.app/api/top-langs?username=WhiteHatCyberus&show_icons=true&locale=en&layout=compact&line_height=20&title_color=7A7ADB&icon_color=2234AE&text_color=D3D3D3&bg_color=0,000000,130F40" width="375"  alt="WhiteHatCyberus"/>

</a>
</div>

<br>
<br>
<br>

-----
